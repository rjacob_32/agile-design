package  
{
	/**
	 * ...
	 * @author Rohan
	 */
	import flash.display.*;
	import flash.events.*;
	public class Board extends Sprite
	{
		
		//private var myGrid:Array;
		// container to hold the grid of dots
		private var grid:Sprite = new Sprite();
		
		// an array that will be populated in the initialization loop
		private var dotArray:Array = new Array();
		
		// graphics
		private var g:Graphics;
		
		// track the dots that are clicked
		private var one:Sprite;
		private var two:Sprite;
		private var d1:Dot;
		private var d2:Dot;
		// track dot selection
		private var dotSelected:Dot;
		//allow click?
		private var allow:Boolean;
		//dot position
		private var count:int;
		private var xpad:int;
		private var ypad:int;
		
		// CONFIGURATION VARS
		//private var count:int;
		// number of columns
		private var col:int;
		// number of rows
		private var row:int;
		// space between the dots
		private var pad:int;
		// dot size
		private var dotSize:Number;
		// line thickness
		private var lineSize:Number;
		
		public function Board()
		{
			trace("11");
			this.allow = false;
			this.dotSelected = null;
			this.col = 4;
			// number of rows
			this.row = 4;
			// space between the dots
			this.pad = 25;
			// dot size
			this.dotSize = 4;
			// line thickness
			this.lineSize = 2;
			//count
			this.count = 1;
			// initialize the grid of dots
			for (var r:int = 0; r < col; r++)
			{
				for (var c:int = 0; c < row; c++)
				{
					// create a dot
					//var dot:Sprite = new Sprite();
					var dot:Dot = new Dot(count)
					//dot.pos = count;
					count++;
					//g = dot.graphics;
					dot.graphics.beginFill(0x333333);
					dot.graphics.drawCircle(0,0,dotSize);
					// position the dot
					dot.x = (dot.width + pad) * c;
					dot.y = (dot.height + pad) * r;
					// hand cursor on mouse over
					dot.buttonMode = true;
					// add the dot to the grid
					grid.addChild(dot);
					// push dot into the array
					dotArray.push(dot);
					//addChild(dotArray);
					// position grid
					//grid.x = stage.stageWidth/2 - grid.width/2;
					//grid.y = stage.stageHeight/2 - grid.height/2;
					grid.x = 50;
					grid.y = 50;
					// register grid for mouse clicks
					grid.addEventListener(MouseEvent.CLICK, onClick);
					
					// add the grid to the displaylist
					addChild(grid);
				}
			}
			//positioning
			this.xpad = dot.width + pad;
			this.ypad = dot.height + pad;
			for (var i: Number = 0; i < dotArray.length; i++)
			{
				this.addEventListener(MouseEvent.CLICK, dotClicked, false, 0, true);
			}
			trace("22");
		}
		
		public function dotClicked(e:MouseEvent):void
		{
			trace(e.target.pos);
			/*if (one)
			{
				d2 = e.target as Sprite;
				trace(d1.pos);
				trace(d2.pos);
			}
			else
			{
				d1 = e.target as Sprite;
			}
			/*trace(e.target.connected);
			trace(e.target.link1)
			if ((e.target.link1) && (e.target.link2) && (e.target.link3) && (e.target.link4))
			{
				e.target.connected = 1;
			}
			if (e.target.link1==null)
			{
				e.target.link1 = 1;
			}
			else if ((e.target.link1)&&(e.target.link2==null))
			{
				e.target.link2 = 1;
			}
			else if ((!e.target.link2)&&(!e.target.link3))
			{
				e.target.link3 = 1;
			}
			else ((!e.target.link3)&&(!e.target.link4))
			{
				e.target.link4 = 1;
			}*/
		}
		public function onClick(e:MouseEvent):void
		{
			// runs on the second click, since one is valid
			if(one)
			{
				// draw a line between one and two
				two = e.target as Sprite;
				if ((((one.x + xpad) == (two.x)) || ((one.x - xpad) == (two.x)))&&(one.y == two.y))
				{
					/*trace(one.x);
					trace(one.y);
					trace(two.x);
					trace(two.y);
					//if(one.x*/
					g = grid.graphics;
					g.lineStyle(lineSize, 0x333333);
					g.moveTo(one.x, one.y);
					g.lineTo(two.x, two.y);
				}
				else if((((one.y + ypad) == (two.y))||((one.y - ypad) == (two.y)))&&(one.x == two.x))
				{
					/*trace(one.x);
					trace(one.y);
					trace(two.x);
					trace(two.y);*/
					g = grid.graphics;
					g.lineStyle(lineSize, 0x333333);
					g.moveTo(one.x, one.y);
					g.lineTo(two.x, two.y);
				}
				else
				{
					trace("Not allowed");
				}
				// nullify the two vars
				one = null;
				two = null;
			}
			// runs on the first click, since one is null
			else
			{
				one = e.target as Sprite;
				//trace(one.pos);
			}
		}
		
	}

}