package
{
	import asunit.textui.TestRunner;
	import flash.display.Sprite;
	import AllTests;
	import flash.events.Event;
	import Board;
	import Dot;
	
	/**
	 * ...
	 * @author Rohan
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) 
			{
				init(null);
				//var unittests:TestRunner = new TestRunner();
				//stage.addChild(unittests);
				//unittests.start(AllTests, null, TestRunner.SHOW_TRACE);
			}
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			var bd:Board = new Board();
			this.addChild(bd);
			// entry point
			//trace("00");
			//var bd:Board = new Board();
			/*var playa:Player = new Player();
			playa.initPlayer(3);
			
			var bx:Box = new Box();
			bx.checkBox();
			*/
		}
		
	}
	
}